package com.afs.restapi.repository;

import com.afs.restapi.model.Employee;
import com.afs.restapi.NotFoundException;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {

    private final List<Employee> employees = new ArrayList<>();
    private final AtomicLong atomicID = new AtomicLong(5L);

    public EmployeeRepository() {

    }

    public List<Employee> findAllEmployee() {
        return employees;
    }

    public Employee findEmployeeById(Long id) {
        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElseThrow(NotFoundException::new);
    }

    public List<Employee> findEmployeeByGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public List<Employee> findByPageAndSize(Integer page, Integer size) {
        return employees.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Employee addEmployee(Employee newEmployee) {
        newEmployee.setId(atomicID.incrementAndGet());
        employees.add(newEmployee);
        return newEmployee;
    }

    public Employee updateEmployeeById(Long id, Employee employee) {
        Employee employeeUpdated = employees.stream().filter(employee1 -> Objects.equals(employee1.getId(), id)).findFirst().orElseThrow();
        employeeUpdated.setAge(employee.getAge());
        employeeUpdated.setGender(employee.getGender());
        employeeUpdated.setName(employee.getName());
        employeeUpdated.setSalary(employee.getSalary());
        return employeeUpdated;
    }

    public void delete(Long id) {
        employees.stream().filter(employee -> employee.getId().equals(id)).findFirst().orElseThrow().setActive(false);
    }

    public void clearAll() {
        employees.clear();
    }

    public List<Employee> getByCompanyId(Long id) {
        return employees.stream().filter(employee -> employee.getCompanyId().equals(id)).collect(Collectors.toList());
    }
}

