package com.afs.restapi.repository;

import com.afs.restapi.model.Company;
import com.afs.restapi.NotFoundException;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {

    private final List<Company> companies = new ArrayList<>();
    private final AtomicLong atomicID = new AtomicLong(5L);

    public CompanyRepository() {
        companies.add(new Company(1L, "OO CL"));
        companies.add(new Company(2L, "Cargo Smart"));

    }

    public List<Company> findAllCompany() {
        return companies;
    }

    public Company findCompanyById(Long id) {
        return companies.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst()
                .orElseThrow(NotFoundException::new);
    }


    public List<Company> findByPageAndSize(Integer page, Integer size) {
        return companies.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Company addCompany(Company newCompany) {
        newCompany.setId(atomicID.incrementAndGet());
        companies.add(newCompany);
        return newCompany;
    }

    public Company updateCompanyById(Long id, Company company) {
        Company companyUpdated = companies.stream().filter(company1 -> Objects.equals(company1.getId(), id)).findFirst().orElseThrow();
        companyUpdated.setName(company.getName());
        return companyUpdated;
    }

    public void deleteCompanyById(Long id) {
        companies.removeIf(company -> company.getId().equals(id));
    }

    public void clearAll() {
        companies.clear();
    }
}
