package com.afs.restapi;

import com.afs.restapi.exception.AgeAndSalaryInvalidException;
import com.afs.restapi.exception.AgeIsInvalidException;
import com.afs.restapi.exception.OnlyEditAgeAndSalaryException;
import com.afs.restapi.exception.OnlyEditIsActiveEmployeeException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class EmployeeServiceTest {
    private final EmployeeRepository employeeRepositoryMock = mock(EmployeeRepository.class);
    @Test
    void should_throw_error_when_addEmployee_given_a_employee_have_invalid_age() {
        //Given
        EmployeeService employeeService = new EmployeeService(employeeRepositoryMock);
        Employee employee = new Employee(1L, "John Smith", 15, "Male", 5000.0);
        //When
        assertThrows(AgeIsInvalidException.class,() -> employeeService.addEmployee(employee));
    }
    @Test
    void should_throw_error_when_addEmployee_given_a_employee_have_invalid_salary_and_age() {
        //Given
        EmployeeService employeeService = new EmployeeService(employeeRepositoryMock);
        Employee employee = new Employee(1L, "John Smith", 31, "Male", 5000.0);

        //When
        assertThrows(AgeAndSalaryInvalidException.class,() -> employeeService.addEmployee(employee));
    }

    @Test
    void should_throw_error_when_updateEmployee_given_a_employee_have_not_active() throws Exception {
        //Given
        EmployeeService employeeService = new EmployeeService(employeeRepositoryMock);
        Employee employee = new Employee(null, "John Smith", 20, "Male", 5000.0);
        Employee newEmployee = new Employee(null,"John Smith",21,"Male",6000.0);
        employee.setActive(false);
        given(employeeRepositoryMock.findEmployeeById(1L)).willReturn(employee);


        //When
        assertThrows(OnlyEditIsActiveEmployeeException.class,() -> employeeService.updateEmployeeById(1L,newEmployee));
    }

    @Test
    void should_throw_error_when_updateEmployee_given_a_employee_have_different_name_and_gender() throws Exception {
        //Given
        EmployeeService employeeService = new EmployeeService(employeeRepositoryMock);
        Employee employee = new Employee(null, "John Smith", 20, "Male", 5000.0);
        employee.setActive(true);
        Employee newEmployee = new Employee(null,"John",21,"Female",6000.0);
        given(employeeRepositoryMock.findEmployeeById(1L)).willReturn(employee);

        //When
        assertThrows(OnlyEditAgeAndSalaryException.class,() -> employeeService.updateEmployeeById(1L,newEmployee));
    }

    @Test
    void should_set_active_false_when_deleteEmployee_given_a_employee() {
        //Given
        EmployeeService employeeService = new EmployeeService(employeeRepositoryMock);
        Employee employee = new Employee(1L, "John Smith", 19, "Male", 5000.0);
        employee.setActive(true);
        given(employeeRepositoryMock.findEmployeeById(1L)).willReturn(employee);
        //When
        employeeService.deleteEmployeeById(1L);
        //Then
        verify(employeeRepositoryMock,times(1)).delete(1L);
    }

}
