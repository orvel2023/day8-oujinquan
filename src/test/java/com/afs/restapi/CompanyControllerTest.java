package com.afs.restapi;

import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerTest {
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private MockMvc client;
    @BeforeEach
    void cleanCompanyDate(){
        companyRepository.clearAll();
    }

    @Test
    void should_return_created_company_when_perform_addCompany_given_a_company_json() throws Exception {
        //Given
        Company oocl = new Company(null,"OOCL");

        String ooclJson = new ObjectMapper().writeValueAsString(oocl);
        //When
        client.perform(MockMvcRequestBuilders.post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(ooclJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("OOCL"));
    }

    @Test
    void should_return_companies_when_getAllCompany_given_companies() throws Exception {
        //Given
        Company oocl = new Company(null,"OOCL");
        companyRepository.addCompany(oocl);
        //When
        client.perform(MockMvcRequestBuilders.get("/companies")) //Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(oocl.getId()))
                .andExpect(jsonPath("$[0].name").value(oocl.getName()));
    }

    @Test
    void should_return_company_find_by_id_when_perform_findCompanyById_given_a_company_id_and_companies() throws Exception {
        //Given
        Company oocl = new Company(null,"OOCL");
        Company cargoSmart = new Company(null,"Cargo Smart");
        companyRepository.addCompany(oocl);
        companyRepository.addCompany(cargoSmart);

        //When
        client.perform(MockMvcRequestBuilders.get("/companies/{id}",cargoSmart.getId())) //Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(cargoSmart.getId()))
                .andExpect(jsonPath("$.name").value(cargoSmart.getName()));
    }

    @Test
    void should_return_updated_company_when_perform_updateCompany_given_a_company_id_and_company_json() throws Exception {
        //Given
        Company cargoSmart = new Company(null,"Cargo");
        Company newCargo = new Company(null,"Cargo Smart");
        companyRepository.addCompany(cargoSmart);
        String newCargoJson = new ObjectMapper().writeValueAsString(newCargo);
        //When
        client.perform(MockMvcRequestBuilders.put("/companies/{id}",cargoSmart.getId())
                .contentType(MediaType.APPLICATION_JSON).content(newCargoJson)) //Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(cargoSmart.getId()))
                .andExpect(jsonPath("$.name").value(newCargo.getName()));
    }

    @Test
    void should_return_null_when_perform_deleteCompany_given_a_company_id() throws Exception {
        //Given
        Company cargoSmart = new Company(null,"Cargo Smart");
        companyRepository.addCompany(cargoSmart);
        //When
        client.perform(MockMvcRequestBuilders.delete("/companies/{id}",cargoSmart.getId())) //Then
                .andExpect(status().isNoContent());
    }

    @Test
    void should_return_companies_when_perform_findByPageAndSize_given_size_and_page() throws Exception {
        //Given
        Company cargoSmart = new Company(null,"Cargo Smart");
        Company oocl = new Company(null,"OOCL");
        Company byteDance = new Company(null,"Byte Dance");
        companyRepository.addCompany(cargoSmart);
        companyRepository.addCompany(oocl);
        companyRepository.addCompany(byteDance);
        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();

        parameters.set("page", "1");
        parameters.set("size", "2");

        //When
        client.perform(MockMvcRequestBuilders.get("/companies").params(parameters)) //Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(cargoSmart.getId()))
                .andExpect(jsonPath("$[0].name").value(cargoSmart.getName()))
                .andExpect(jsonPath("$[1].id").value(oocl.getId()))
                .andExpect(jsonPath("$[1].name").value(oocl.getName()));
    }

    @Test
    void should_return_employees_when_perform_getEmployeesByCompanyId_given_companyId_and_employees() throws Exception {
        //Given
        Company cargoSmart = new Company(null,"Cargo Smart");
        companyRepository.addCompany(cargoSmart);
        Employee rock = new Employee(null,"rock",30,"Female",4000.0,2L);
        Employee jim = new Employee(null,"jim",23,"Female",14000.0,cargoSmart.getId());
        Employee vicky = new Employee(null,"vicky",23,"Female",14000.0,cargoSmart.getId());
        employeeRepository.addEmployee(jim);
        employeeRepository.addEmployee(vicky);
        employeeRepository.addEmployee(rock);
        //When
        client.perform(MockMvcRequestBuilders.get("/companies/{id}/employees",cargoSmart.getId())) //Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(jim.getId()))
                .andExpect(jsonPath("$[0].name").value(jim.getName()))
                .andExpect(jsonPath("$[1].id").value(vicky.getId()))
                .andExpect(jsonPath("$[1].name").value(vicky.getName()));
    }

}
