# O
  - Code Review : I found myself with a lot of java features I didn't understand, such as auto-loading, deserialization, etc., and a lot of comments I didn't use.
  - Spring Boot Test ： I learned how to write Spring Boot tests and mock processes, learned a lot about the use of annotations and assertions, and learned a lot about various parameter passing methods.
  - Layered Architecture : Today, I learned how to develop the project code in layers in the project, so that each functional module can be decoupled, and the project architecture with high cohesion and low coupling can be realized.
  - Test pyramid : Today we learned about the relationship between UI testing, Service testing, and Unit testing. As well as their test success, the best practice in software should be a lot of Unit testing, a moderate amount of service testing, and a small amount of UI testing

# R
  -  Challenging!
# I
  - I think today's let me learned the new technology of Spring Boot Test and many java annotations. Gave me a deeper understanding of layered architecture and layered testing.
# D
  - Learn more about using java annotations.
  - Learn a more comprehensive java exception handling mechanism.
  - Learn more about Spring Boot Test and practice it.