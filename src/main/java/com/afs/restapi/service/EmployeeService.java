package com.afs.restapi.service;

import com.afs.restapi.exception.OnlyEditAgeAndSalaryException;
import com.afs.restapi.exception.OnlyEditIsActiveEmployeeException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.exception.AgeAndSalaryInvalidException;
import com.afs.restapi.exception.AgeIsInvalidException;
import com.afs.restapi.repository.EmployeeRepository;

import java.util.List;
import java.util.Objects;

public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> findAllEmployee() {
        return employeeRepository.findAllEmployee();
    }

    public Employee findEmployeeById(Long id) {
        return employeeRepository.findEmployeeById(id);
    }

    public List<Employee> findEmployeeByGender(String gender) {
        return employeeRepository.findEmployeeByGender(gender);
    }

    public List<Employee> findByPageAndSize(Integer page, Integer size) {
        return employeeRepository.findByPageAndSize(page,size);
    }

    public Employee addEmployee(Employee newEmployee)  {
        if(!newEmployee.isValid()){
            throw new AgeIsInvalidException();
        }
        if(newEmployee.ageAndSalaryIsInValid()){
            throw new AgeAndSalaryInvalidException();
        }
        newEmployee.setActive(true);
        return employeeRepository.addEmployee(newEmployee);
    }

    public Employee updateEmployeeById(Long id, Employee employee) {
        Employee employeeToUpdate = findEmployeeById(id);
        if(!Objects.equals(employee.getName(), employeeToUpdate.getName()) || !Objects.equals(employee.getGender(), employeeToUpdate.getGender()))
        {
            throw new OnlyEditAgeAndSalaryException();
        }
        if(!employeeToUpdate.isActive()){
            throw new OnlyEditIsActiveEmployeeException();
        }
            return employeeRepository.updateEmployeeById(id,employee);
    }

    public void deleteEmployeeById(Long id) {
        Employee employee = employeeRepository.findEmployeeById(id);
        employeeRepository.delete(employee.getId());
    }

    public void clearAll() {
        employeeRepository.clearAll();
    }
}
