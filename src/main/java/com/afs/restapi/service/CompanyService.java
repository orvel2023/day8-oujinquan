package com.afs.restapi.service;

import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;

import java.util.List;

public class CompanyService {

    private final CompanyRepository companyRepository;
    private final EmployeeRepository employeeRepository;

    public CompanyService(CompanyRepository companyRepository, EmployeeRepository employeeRepository) {
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
    }

    public List<Company> findAllCompany() {
        return companyRepository.findAllCompany();
    }

    public Company findCompanyById(Long id) {
        return companyRepository.findCompanyById(id);
    }


    public List<Company> findByPageAndSize(Integer page, Integer size) {
        return companyRepository.findByPageAndSize(page,size);
    }

    public Company addCompany(Company newCompany)  {
        return companyRepository.addCompany(newCompany);
    }

    public Company updateCompanyById(Long id, Company company) {
        Company companyToUpdate = findCompanyById(id);
        companyToUpdate.setName(company.getName());
        return companyRepository.updateCompanyById(id,companyToUpdate);
    }

    public void deleteCompanyById(Long id) {
        Company company = companyRepository.findCompanyById(id);
        companyRepository.deleteCompanyById(company.getId());
    }
    public List<Employee> getEmployeesByCompanyId(Long id)  {
        return employeeRepository.getByCompanyId(id);
    }
    public void clearAll() {
        companyRepository.clearAll();
    }
}