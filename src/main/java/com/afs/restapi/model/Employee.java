package com.afs.restapi.model;

public class Employee {
    private Long id;
    private String name;
    private Integer age;
    private String gender;
    private Double salary;
    private boolean active;
    private Long companyId;

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Employee() {
    }

    public Employee(Long id, String name, Integer age, String gender, Double salary) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
    }
    public Employee(Long id, String name, Integer age, String gender, Double salary,Long companyId) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
        this.companyId = companyId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public void merge(Employee employee) {
        this.age = employee.age;
        this.salary = employee.salary;
    }

    public boolean isValid() {
        return age >=18 && age<=65;
    }

    public boolean ageAndSalaryIsInValid() {
        return age >= 30 && salary <= 20000;
    }
}
