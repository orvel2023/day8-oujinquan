package com.afs.restapi;

import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Collections;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private MockMvc client;
    @BeforeEach
    void cleanEmployeeDate(){
        employeeRepository.clearAll();
    }
    @Test
    void should_return_employees_when_getAllEmployees_given_employees() throws Exception {
        //Given
        Employee join = new Employee(null,"John Smith",32,"Male",5000.0);
        employeeRepository.addEmployee(join);
        //When
        client.perform(MockMvcRequestBuilders.get("/employees")) //Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(join.getId()))
                .andExpect(jsonPath("$[0].name").value(join.getName()))
                .andExpect(jsonPath("$[0].age").value(join.getAge()))
                .andExpect(jsonPath("$[0].gender").value(join.getGender()))
                .andExpect(jsonPath("$[0].salary").value(join.getSalary()));

    }

    @Test
    void should_return_employees_filter_by_gender_when_perform_findEmployeesByGender_given_two_employees_different() throws Exception {
        //Given
        Employee join = new Employee(null,"John Smith",32,"Male",5000.0);
        Employee jim = new Employee(null,"jim",20,"Female",4000.0);
        employeeRepository.addEmployee(join);
        employeeRepository.addEmployee(jim);
        //When
        client.perform(MockMvcRequestBuilders.get("/employees").param("gender","Female")) //Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(jim.getId()))
                .andExpect(jsonPath("$[0].name").value(jim.getName()))
                .andExpect(jsonPath("$[0].age").value(jim.getAge()))
                .andExpect(jsonPath("$[0].gender").value(jim.getGender()))
                .andExpect(jsonPath("$[0].salary").value(jim.getSalary()));

    }

    @Test
    void should_return_created_employee_when_perform_insertEmployees_given_a_employee_json() throws Exception {
        //Given
        Employee jim = new Employee(null,"jim",20,"Female",4000.0);

        String jimJson = new ObjectMapper().writeValueAsString(jim);
        //When
        client.perform(MockMvcRequestBuilders.post("/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jimJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("jim"))
                .andExpect(jsonPath("$.age").value(20))
                .andExpect(jsonPath("$.gender").value("Female"))
                .andExpect(jsonPath("$.active").value(true))
                .andExpect(jsonPath("$.salary").value(4000.0));
    }
    @Test
    void should_return_employee_find_by_id_when_perform_findEmployeesById_given_a_employee_id_and_employees() throws Exception {
        //Given
        Employee jim = new Employee(null,"jim",20,"Female",4000.0);
        Employee join = new Employee(null,"John Smith",32,"Male",5000.0);
        employeeRepository.addEmployee(join);
        employeeRepository.addEmployee(jim);

        //When
        client.perform(MockMvcRequestBuilders.get("/employees/{id}",jim.getId())) //Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(jim.getId()))
                .andExpect(jsonPath("$.name").value(jim.getName()))
                .andExpect(jsonPath("$.age").value(jim.getAge()))
                .andExpect(jsonPath("$.gender").value(jim.getGender()))
                .andExpect(jsonPath("$.salary").value(jim.getSalary()));
    }

    @Test
    void should_return_updated_employee_when_perform_updateEmployee_given_a_employee_id_and_employee_json() throws Exception {
        //Given
        Employee jim = new Employee(null,"jim",20,"Female",4000.0);
        Employee newJim = new Employee(null,"jim",32,"Female",10000.0);
        jim.setActive(true);
        employeeRepository.addEmployee(jim);
        employeeRepository.addEmployee(newJim);
        String jimJson = new ObjectMapper().writeValueAsString(newJim);
        //When
        client.perform(MockMvcRequestBuilders.put("/employees/{id}",jim.getId())
                .contentType(MediaType.APPLICATION_JSON).content(jimJson)) //Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(jim.getId()))
                .andExpect(jsonPath("$.name").value(newJim.getName()))
                .andExpect(jsonPath("$.age").value(newJim.getAge()))
                .andExpect(jsonPath("$.gender").value(newJim.getGender()))
                .andExpect(jsonPath("$.salary").value(newJim.getSalary()));
    }
    @Test
    void should_return_null_when_perform_deleteEmployee_given_a_employee_id() throws Exception {
        //Given
        Employee jim = new Employee(null,"jim",20,"Female",4000.0);
        employeeRepository.addEmployee(jim);
        //When
        client.perform(MockMvcRequestBuilders.delete("/employees/{id}",jim.getId())) //Then
                .andExpect(status().isNoContent());
    }
    @Test
    void should_return_employees_when_perform_findByPageAndSize_given__size_and_page() throws Exception {
        //Given
        Employee jim = new Employee(null,"jim",23,"Female",14000.0);
        Employee vicky = new Employee(null,"vicky",23,"Female",14000.0);
        Employee ace = new Employee(null,"ace",26,"Female",24000.0);
        Employee rock = new Employee(null,"rock",30,"Female",4000.0);
        employeeRepository.addEmployee(jim);
        employeeRepository.addEmployee(vicky);
        employeeRepository.addEmployee(ace);
        employeeRepository.addEmployee(rock);
        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();

        parameters.set("page", "1");
        parameters.set("size", "2");

        //When
        client.perform(MockMvcRequestBuilders.get("/employees").params(parameters)) //Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(jim.getId()))
                .andExpect(jsonPath("$[0].name").value(jim.getName()))
                .andExpect(jsonPath("$[1].id").value(vicky.getId()))
                .andExpect(jsonPath("$[1].name").value(vicky.getName()));
    }
}
